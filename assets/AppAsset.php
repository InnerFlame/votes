<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    
    public $baseUrl = '@web';
    
    public $css = [
        'plugins/app/css/site.css',
        # Global stylesheets
        'plugins/bootstrap-limitless/assets/css/icons/icomoon/styles.css',
        'plugins/bootstrap-limitless/assets/css/minified/core.min.css',
        'plugins/bootstrap-limitless/assets/css/minified/components.min.css',
        'plugins/bootstrap-limitless/assets/css/minified/colors.min.css',
        # /global stylesheets
    ];
    
    public $js = [
        # Core JS files
        'plugins/bootstrap-limitless/assets/js/plugins/loaders/pace.min.js',
        'plugins/bootstrap-limitless/assets/js/plugins/loaders/blockui.min.js',
        # /core JS files

        # Theme JS files
        'plugins/bootstrap-limitless/assets/js/plugins/visualization/d3/d3.min.js',
        'plugins/bootstrap-limitless/assets/js/plugins/visualization/d3/d3_tooltip.js',
        'plugins/bootstrap-limitless/assets/js/plugins/forms/styling/switchery.min.js',
        'plugins/bootstrap-limitless/assets/js/plugins/forms/styling/uniform.min.js',
        'plugins/bootstrap-limitless/assets/js/plugins/forms/selects/bootstrap_multiselect.js',
        'plugins/bootstrap-limitless/assets/js/plugins/ui/moment/moment.min.js',
        'plugins/bootstrap-limitless/assets/js/plugins/pickers/daterangepicker.js',

        'plugins/bootstrap-limitless/assets/js/core/app.js',
        'plugins/bootstrap-limitless/assets/js/pages/dashboard.js',
        # /theme JS files
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
