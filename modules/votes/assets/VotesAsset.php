<?php

namespace app\modules\votes\assets;

use yii\web\AssetBundle;

class VotesAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/votes/assets/';

    public $css = [
//        'css/votes.css',
    ];

    public $js = [
//        'js/votes.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
