<?php

namespace app\modules\votes\models\answers;

use app\modules\votes\models\DefaultModel;
use app\modules\votes\models\questions\Questions;
use app\modules\votes\models\votes\Votes;
use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $title
 *
 * @property Questions $question
 * @property Votes[] $votes
 */
class Answers extends DefaultModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'title'       => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     * @return AnswersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnswersQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(Votes::className(), ['answer_id' => 'id']);
    }

    /*
     |--------------------------------------------------------------------------
     | Methods
     |--------------------------------------------------------------------------
     */
}
