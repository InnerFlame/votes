<?php

namespace app\modules\votes\models\users;

use app\modules\votes\models\DefaultModel;
use app\modules\votes\models\questions\Questions;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $created_at
 *
 * @property Questions[] $questions
 */
class Users extends DefaultModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'created_at'], 'required'],
            [['created_at'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 32],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'email'      => Yii::t('app', 'Email'),
            'password'   => Yii::t('app', 'Password'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Questions::className(), ['user_id' => 'id']);
    }


    /*
     |--------------------------------------------------------------------------
     | Methods
     |--------------------------------------------------------------------------
     */
}
