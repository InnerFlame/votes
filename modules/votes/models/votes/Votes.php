<?php

namespace app\modules\votes\models\votes;

use app\modules\votes\models\answers\Answers;
use app\modules\votes\models\DefaultModel;
use Yii;

/**
 * This is the model class for table "votes".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $answer_id
 * @property integer $count
 *
 * @property Answers $answer
 */
class Votes extends DefaultModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'votes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer_id', 'count'], 'integer'],
            [['ip'], 'string', 'max' => 32],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answers::className(), 'targetAttribute' => ['answer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app', 'ID'),
            'ip'        => Yii::t('app', 'Ip'),
            'answer_id' => Yii::t('app', 'Answer ID'),
            'count'     => Yii::t('app', 'Count'),
        ];
    }

    /**
     * @inheritdoc
     * @return VotesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VotesQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answers::className(), ['id' => 'answer_id']);
    }

    /*
     |--------------------------------------------------------------------------
     | Methods
     |--------------------------------------------------------------------------
     */
}
