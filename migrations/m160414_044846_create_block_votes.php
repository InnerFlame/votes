<?php

use yii\db\Migration;

class m160414_044846_create_block_votes extends Migration
{
    public function safeUp()
    {
        $tableOptions = "";

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password' => $this->string(32)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),// FK
            'link' => $this->string(32),
            'title' => $this->string(255),
        ], $tableOptions);

        $this->createTable('answers', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer(),// FK
            'title' => $this->string(255),
        ], $tableOptions);

        $this->createTable('votes', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(32),
            'answer_id' => $this->integer(),// FK
            'count' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('idx-questions-user_id', 'questions', 'user_id');
        $this->createIndex('idx-answers-question_id', 'answers', 'question_id');
        $this->createIndex('idx-votes-answer_id', 'votes', 'answer_id');

        $this->addForeignKey('fk-questions-user_id', 'questions', 'user_id', 'users', 'id', 'CASCADE');
        $this->addForeignKey('fk-answers-question_id', 'answers', 'question_id', 'questions', 'id', 'CASCADE');
        $this->addForeignKey('fk-votes-answer_id', 'votes', 'answer_id', 'answers', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('users');
        $this->dropTable('questions');
        $this->dropTable('answers');
        $this->dropTable('votes');
    }
}
