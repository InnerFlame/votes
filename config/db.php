<?php

//return [
//    'class' => 'yii\db\Connection',
//    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
//    'username' => 'root',
//    'password' => '',
//    'charset' => 'utf8',
//];
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;port=5432;dbname=votes',
    'schemaMap' => [
        'pgsql' => [
            'class' => 'yii\db\pgsql\Schema',
            'defaultSchema' => 'votes' //specify your schema here
        ]
    ],
    'username' => 'postgres',
    'password' => 'postgres',
    'charset' => 'utf8',
    'on afterOpen' => function ($event)
    {
        $event->sender->createCommand("SET search_path TO votes")->execute();
    }
];
